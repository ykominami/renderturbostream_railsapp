class CreateCableMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :cable_messages do |t|
      t.string :title

      t.timestamps
    end
  end
end
