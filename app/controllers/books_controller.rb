class BooksController < ApplicationController
  before_action :set_book, only: %i[ show edit update destroy ]

  @controller_turbo_frame_tag = 'books-box'

  # GET /books or /books.json
  def index
    @books = Book.all
  end

  # GET /books/1 or /books/1.json
  def show
  end

  # GET /books/new
  def new
    @book = Book.new
  end

  # GET /books/1/edit
  def edit
  end

  # POST /books or /books.json
  def create
    @book = Book.new(book_params)
    turbo_stream_save(
      @book.save,
      if_success_redirect_to: books_path
    )
  end

  # PATCH/PUT /books/1 or /books/1.json
  def update
    turbo_channel_save(
      @book.update(book_params),
      add: [
        {
          target_id: 'flash-box',
          action: 'prepend',
          partial: 'layouts/flash',
          locals: { message: 'ADDITIONAL MESSAGE!!', success: true }
        },
        ['add_css_class', '#colored-element', 'background-green']
      ]
    )
  end

  def test_nil
    turbo_channel_save(
      true,
    )
  end

  # DELETE /books/1 or /books/1.json
  def destroy
    turbo_stream_save(
      @book.destroy,
      if_success_redirect_to: books_path
    )
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_book
    @book = Book.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def book_params
    params.require(:book).permit(:title)
  end

end
