class ArticlesController < ApplicationController
  before_action :set_article, only: %i[ show edit update destroy ]

  # GET /articles or /articles.json
  def index
    @articles = Article.all
    # testmethod
  end

  # GET /articles/1 or /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles or /articles.json
  def create
    @article = Article.new(article_params)

    turbo_stream_save(
      @article.save,
      if_success_turbo_redirect_to: ->{article_path(@article)},
      target_id: 'form',
      partial: 'form'
    )
  end

  # PATCH/PUT /articles/1 or /articles/1.json
  def update
    turbo_stream_save(
      @article.update(article_params),
      if_success_turbo_redirect_to: (params['no_redirection'] == 'true' ? nil : articles_path),
      target_id: 'form',
      partial: 'form'
    )
  end

  # DELETE /articles/1 or /articles/1.json
  def destroy
    turbo_stream_save(
      @article.destroy,
      if_success_redirect_to: articles_path
    )
  end

  def check
    render layout: false
  end

  def check_erb
    render layout: false
  end

  def check_file
    p = Rails.root.join('app', 'test_outside_views', 'check_file.turbo_stream.erb')
    raise "path not working: «#{p}»" unless File.exist?(p)
    respond_to do |format|
      format.turbo_stream { render file: p, layout: false }
    end
  end

  def test_stream_partials
    ary_js = params.permit(:partials_array)[:partials_array]
    if ary_js.present?
      ary = JSON.parse(ary_js)
    else
      ary = [
        {
          target_id: 'flash-box',
          partial: 'flash_box_append',
          locals: { content: "FIRST streamed partial #{SecureRandom.hex(3)}" },
          action: :append
        },
        {
          target: '#flash-box',
          partial: 'flash_box_append',
          locals: { content: "SECOND streamed partial #{SecureRandom.hex(3)}" },
          action: :append
        }
      ]
    end
    render_turbo_stream(
      ary
    )
  end

  def test_no_stream
    @article = Article.new(article_params)

    turbo_stream_save(
      @article.save,
      target_id: nil
    )
    redirect_to root_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def article_params
    params.require(:article).permit(:title, :description)
  end
end
