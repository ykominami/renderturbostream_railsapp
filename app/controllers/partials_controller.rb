class PartialsController < ApplicationController

  def index

  end

  def test_stream_partial
    @article = Article.new(title: 'Article.title is working')
    @var = 'working'
    stream_partial('variables_test', locals: {title: 'working'})
  end

  def test_channel
    @article = Article.new(title: 'Article.title is working')
    @var = 'hello'
    if Article.first.present?
      Article.first.update(title: 'test-article-title')
    else
      Article.create(title: 'test-article-title')
    end
    render_to_all(
      partial: 'variables_test',
      locals: {
        title: 'working'
      }
    )
    render text: nil, layout: false
  end

  def traditional
    @test_traditional_partial = true
    @article = Article.new(title: 'Article.title is working')
    @var = 'hello'
    render :index
  end

  def traditional2
    @test_traditional2_partial = true
    @article = Article.new(title: 'Article.title is working')
    @var = 'hello'
    render :index
  end

  def stream_by_params
    stream_partial(
      params['partial'],
      id: params['target_id']
    )
  end

  def channel_by_params
    render_to_all(
      params['target_id'],
      partial: params['partial']
    )
  end

end