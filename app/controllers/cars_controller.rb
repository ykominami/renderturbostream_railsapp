class CarsController < ApplicationController

  def page

  end

  def index
    a = 1
  end

  def new
    @car = Car.new
  end

  def edit
    @car = Car.find(params['id'])
  end

  def update
    @car = Car.find(params['id'])
    turbo_stream_save(
      @car.update(car_params),
      if_success_redirect_to: cars_path,
      partial: 'form'
    )
    # target-id: turbo_stream_save fetches it from the 'form'!!
  end

  def create
    @car = Car.new(car_params)
    turbo_stream_save(
      @car.save,
      if_success_redirect_to: cars_path,
    )
  end

  def destroy
    @car = Car.find(params['id'])
    turbo_stream_save(
    @car.destroy,
    if_success_redirect_to: cars_path
    )
  end

  private

  def car_params
    params.require(:car).permit(:title, :id, :description)
  end
end