class Car < ApplicationRecord
  validates :title, presence: true
end
