
console.log('Vite ⚡️ Rails')

import '@hotwired/turbo-rails'

import TurboPower from 'turbo_power'
TurboPower.initialize(Turbo.StreamActions)

// Stimulus
import { initStimulus } from "vite-stimulus-initializer";
const controllers = import.meta.glob('../javascript/**/*-controller.js', { eager: true })
initStimulus(controllers, 2, { debug: true, exclude: ['components', 'views', 'layout'], folderSeparator: '-'  })

// Svelte
import { initSvelte } from "vite-svelte-initializer";
const apps = import.meta.glob('../javascript/components/**/*.svelte', { eager: true })
initSvelte(apps, 2, { debug: true, exclude: ['components'], folderSeparator: '-' })
