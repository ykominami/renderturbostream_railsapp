class FooPresenter

  # https://stackoverflow.com/questions/27441409/rails-partial-locals-in-helper
  def initialize(object, template)
    @object, @template = object, template
  end

  def amazing_foo
    @template.content_tag :div, class: 'foo' do
      "#{@object}: Wow! this is incredible!"
    end
  end
end
