
# README

This is a Rails 7 project that uses the functionality of the [render_turbo_stream gem](https://rubygems.org/gems/render_turbo_stream) along with the [turbo_power gem](https://github.com/marcoroth/turbo_power).

Tests are written in rspec and capybara, database is sqlite and frontend engine is vite.

Views, as quick and dirty, are scaffolding for articles and one to demonstrate actions like replace browser url or replace a css class directly from the controller.

There is also a demonstration of how to run svelte and a javascript ajax post call properly on turbo.

# Installation

Install ruby

You may need to include the `render_turbo_stream` gem accordingly. Because I have all this locally on my machine, but have not tested the installation from the outside.

Inside the app directory, run:

```ruby
bundle install
rails db:migrate
npm i
```

# Usage

As runner i have foreman, so start command should be anything like

```ruby
foreman start -f Procfile.dev
```

And it should work.


