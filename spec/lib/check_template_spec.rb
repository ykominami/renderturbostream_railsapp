require 'rails_helper'

RSpec.describe 'check partials contents', type: :model do

  it 'no check for instance variables' do
    lib = RenderTurboStream::CheckTemplate.new(partial: 'partials/variables_test', available_instance_variables: nil, action: :replace)
    instance_variables = lib.templates_instance_variables
    expect(instance_variables).to include(:@var, :@article)
  end

  it 'check for instance variables' do
    lib = RenderTurboStream::CheckTemplate.new(partial: 'partials/variables_test', action: :replace)
    instance_variables = lib.templates_instance_variables
    expect(instance_variables).to eq([:@article, :@var])
  end

  it 'cache on production' do
    allow_any_instance_of(RenderTurboStream::CheckTemplate).to receive(:production?).and_return(true)

    lib = RenderTurboStream::CheckTemplate.new(template: 'partials/test_stream_partial.turbo_stream.haml', action: :replace)
    expect(lib.instance_variable_get('@nested_partials_done').length).to eq(3)

    lib = RenderTurboStream::CheckTemplate.new(template: 'partials/test_stream_partial.turbo_stream.haml', action: :replace)
    expect(lib.instance_variable_get('@nested_partials_done')).to eq(nil)
  end

  it 'no cache on development' do
    allow_any_instance_of(RenderTurboStream::CheckTemplate).to receive(:production?).and_return(false)

    lib = RenderTurboStream::CheckTemplate.new(template: 'partials/test_stream_partial.turbo_stream.haml', action: :replace)
    expect(lib.instance_variable_get('@nested_partials_done').length).to eq(3)

    lib = RenderTurboStream::CheckTemplate.new(template: 'partials/test_stream_partial.turbo_stream.haml', action: :replace)
    expect(lib.instance_variable_get('@nested_partials_done').length).to eq(3)
  end

  it 'raise error if relative path not full' do
    expect do
      RenderTurboStream::CheckTemplate.new(partial: 'variables_test', available_instance_variables: nil, action: :replace)
    end.to raise_exception(RuntimeError, /Partial or template path must always be specified with the controller path/)
  end
  
end

