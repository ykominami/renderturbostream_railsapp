require 'rails_helper'

RSpec.describe 'Libs', type: :model do

  context 'channel' do

    before(:each) do
      define_channel
    end

    it 'invalid target_id' do
      r = RenderTurboStream::Test::Request::Libs.new(@response).select_responses(
        'my-channel',
        'X-my-id',
        'replace'
      ) { |e| e.css('div').inner_html.include?('hello') }
      expect(r[:responses].length).to eq(0)
    end

    it 'invalid channel' do
      r = RenderTurboStream::Test::Request::Libs.new(@response).select_responses(
        'X-my-channel',
        'my-id',
        'replace'
      ) { |e| e.css('div').inner_html.include?('hello') }
      expect(r[:responses].length).to eq(0)
    end

    it 'invalid action' do
      r = RenderTurboStream::Test::Request::Libs.new(@response).select_responses(
        'my-channel',
        'my-id',
        'X-replace'
      ) { |e| e.css('div').inner_html.include?('hello') }
      expect(r[:responses].length).to eq(0)
    end

    it 'invalid html content' do
      r = RenderTurboStream::Test::Request::Libs.new(@response).select_responses(
        'my-channel',
        'my-id',
        'replace'
      ) { |e| e.css('div').inner_html.include?('X-hello') }
      expect(r[:responses].length).to eq(0)
    end

  end

  context 'check type' do

    it 'valid channel, check type stream' do
      define_channel
      r = RenderTurboStream::Test::Request::Libs.new(@response).select_responses(
        'my-channel',
        'my-id',
        'replace',
      ) { |e| e.css('div').inner_html.include?('hello') }
      expect(r[:responses].length).to eq(0)
    end

  end

  context 'prohibit multiple to same target' do

    it '2 responses to 2 different ids' do

      response = ActionDispatch::Response.new
      response.headers['turbo-response-test-1'] = { target: '#my-id', action: 'replace', type: 'stream-partial' }.to_json
      response.headers['turbo-response-test-2'] = { target: '#my-id2', action: 'replace', type: 'stream-partial' }.to_json

      r = RenderTurboStream::Test::Request::Libs.new(response).select_responses(
        nil,
        '#my-id',
        'replace'
      )
      expect(r[:responses].length).to eq(1)
    end

    it '2 responses to same id with prohibited action' do

      response = ActionDispatch::Response.new
      response.headers['turbo-response-test-1'] = { target: '#my-id', action: 'replace', type: 'stream-partial' }.to_json
      response.headers['turbo-response-test-2'] = { target: '#my-id', action: 'replace', type: 'stream-partial' }.to_json

      r = RenderTurboStream::Test::Request::Libs.new(response).select_responses(
        nil,
        'my-id',
        'replace'
      )
      expect(r[:responses].length).to eq(0)
    end

    it '2 responses to same id with no prohibited action' do

      response = ActionDispatch::Response.new
      response.headers['turbo-response-test-1'] = { target: '#my-id', action: 'append', type: 'stream-partial' }.to_json
      response.headers['turbo-response-test-2'] = { target: '#my-id', action: 'append', type: 'stream-partial' }.to_json

      r = RenderTurboStream::Test::Request::Libs.new(response).select_responses(
        nil,
        '#my-id',
        nil
      )
      expect(r[:responses].length).to eq(2)
    end
  end

  def define_channel
    @response = ActionDispatch::Response.new
    js = [
      {
        channel: 'my-channel',
        target: '#my-id',
        action: 'replace',
        html_response: '<div>hello</div>',
        type: 'channel-partial'
      }
    ].to_json
    allow(@response).to receive(:body).and_return("'<div id='rendered-partials'>#{js}</div>'")
  end


end
