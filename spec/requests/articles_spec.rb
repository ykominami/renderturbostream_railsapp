require 'rails_helper'

RSpec.describe "Articles", type: :request do

  let(:valid_params) { { article: { title: 'my-next-article', desciption: 'abc' } } }
  let(:invalid_params) { { article: { title: '', desciption: 'abc' } } }
  let(:article) { FactoryBot.create :article }

  it 'expect successful updated (no redirection!) and last_saved_object' do
    patch article_path(article, params: valid_params.merge(no_redirection: 'true'))
    assert_stream_response('flash-box', action: :prepend) { |r| r.css('.callout.success').inner_html.include?('Article successfully updated') }
    expect(session['last_saved_object']).to eq(article.to_global_id)
  end

  it 'expect successful updated with turbo-redirection' do
    patch article_path(article, params: valid_params)
    assert_turbo_redirect_to(articles_path)
  end

  it 'update failed' do
    patch article_path(article, params: invalid_params)
    expect(response.status).to eq(422)
    assert_stream_response('form') { |e| e.css('.field_with_errors').inner_html.include?('title') }
  end

  it 'create failed' do
    post articles_path(params: invalid_params)
    assert_once_targeted('form', 'flash-box')
    assert_stream_response('flash-box') { |r| r.css('.callout.alert').inner_html.include?('Article could not be created') }
    assert_stream_response('form')
  end

  it 'create success' do
    post articles_path(params: valid_params)
    assert_turbo_redirect_to(article_path(Article.last))
  end

  it 'stream partials without params' do
    post test_stream_partials_articles_path
    expect(turbo_targets.length).to eq(1)
    assert_stream_response('flash-box', count: 2)
    assert_stream_response('flash-box') { |p| p.inner_html.include?('FIRST') }
    assert_stream_response('flash-box') { |p| p.inner_html.include?('SECOND') }
    assert_stream_response('flash-box', count: 0) { |p| p.inner_html.include?('SECONDx') }
  end

  it 'stream partials' do
    post test_stream_partials_articles_path(params: { partials_array: [{ target_id: 'flash-box', partial: 'flash_box', locals: { content: 'my-flash' } }].to_json })
    expect(turbo_targets.length).to eq(1)
    assert_stream_response('flash-box') { |p| p.inner_html.include?('my-flash') }
  end

  it 'partial double rendered to same place' do
    ary = [
      { target_id: 'flash-box', partial: 'flash_box', action: 'append', locals: { content: 'first-flash' } },
      { target_id: 'flash-box', partial: 'flash_box', action: 'append', locals: { content: 'second-flash' } }
    ]
    post test_stream_partials_articles_path(params: { partials_array: ary.to_json })
    expect(turbo_targets.length).to eq(1)
    assert_stream_response('flash-box', count: 2)
    assert_stream_response('flash-box', count: 1) { |p| p.inner_html.include?('second-flash') }
  end

  it 'response with multiple flashes' do
    ary = [
      { target_id: 'flash-box', partial: 'layouts/flash', action: 'append', locals: { message: 'Something went wrong', success: false } },
      { target_id: 'flash-box', partial: 'layouts/flash', action: 'append', locals: { message: 'All perfect', success: true } }
    ]
    post test_stream_partials_articles_path(params: { partials_array: ary.to_json })

    expect(turbo_targets.length).to eq(1)

    assert_stream_response('flash-box') do |p|
      p.css('.callout.success').inner_html.include?('All perfect')
    end

    assert_stream_response('flash-box') do |p|
      p.css('.callout.alert').inner_html.include?('Something went wrong')
    end
  end

  it 'hash with invalid content' do
    ary1 = [
      'xx'
    ]
    expect {
      post test_stream_partials_articles_path(params: { partials_array: ary1.to_json })
    }.to raise_error(RuntimeError, /ERROR render_turbo_stream invalid type/)
  end

  it 'empty element should not raise an error' do
    ary1 = [
      ''
    ]
    post test_stream_partials_articles_path(params: { partials_array: ary1.to_json })
    expect(response.status).to eq(200)
  end

  it 'invalid type (not hash nor array)' do
    ary1 = [
      'my-string'
    ]
    expect {
      post test_stream_partials_articles_path(params: { partials_array: ary1.to_json })
    }.to raise_error(RuntimeError, /ERROR render_turbo_stream invalid type/)
  end

  it 'partial not existing' do
    ary = [
      { target_id: 'flash-box', partial: 'partial_not_existing', locals: { content: 'next-flash' } }
    ]
    expect {
      post test_stream_partials_articles_path(params: { partials_array: ary.to_json })
    }.to raise_error(ActionView::Template::Error, /Missing partial articles\/_partial_not_existing/)
  end

  it 'check stream-response action' do
    attr = {
      target_id: 'flash-box',
      partial: 'layouts/flash',
      locals: { message: 'next-flash' }
    }
    post test_stream_partials_articles_path(params: { partials_array: [attr.merge(action: :replace)].to_json })
    assert_stream_response('flash-box', action: :replace)
  end

  it 'delete Article' do
    a = FactoryBot.create :article
    delete article_path(a)
    expect(response).to redirect_to(articles_path)
  end

end
