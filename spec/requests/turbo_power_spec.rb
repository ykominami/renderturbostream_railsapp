require 'rails_helper'

RSpec.describe "/users", type: :request do

  it 'make red' do
    green = [['remove_css_class', '#colored-element', 'background-red'], ['add_css_class', '#colored-element', 'background-green']]
    post action_turbo_power_index_path(params: { ary: green.to_json })


    assert_stream_action('remove_css_class') do |args|
      args == ["#colored-element", "background-red"]
    end
  end

end
