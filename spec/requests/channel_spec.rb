require 'rails_helper'

RSpec.describe "Channel", type: :request do



  it 'expect successful saved' do
    u = FactoryBot.create(:user)

    post channel_redirect2_path

    expect(turbo_targets.length).to eq(1)
    assert_channel_to_me(u, 'flash-box')
    assert_channel_to_me(u, 'flash-box') { |p| p.css('.callout.success').inner_html.to_s.include?('Article successfully created') }
  end

  it 'template' do
    post channel_cable_template_path
    assert_channel_to_all('template-box') { |p| p.inner_html.to_s.include?('greetings from locals') }
  end

  it 'make red to all' do
    post channel_make_red_path
    expect(turbo_targets.length).to eq(1)
    expect(all_turbo_responses.length).to eq(2)
    assert_action_to_all('add_css_class'){|args| args == ["#colored-element", "background-red"] }
    assert_action_to_all('remove_css_class'){|args| args.last == 'background-green' }
  end

  it 'make red to me' do
    u = FactoryBot.create(:user)
    post channel_make_red_to_me_path
    assert_action_to_me(u, 'add_css_class')
    #assert_action_to_me(u, 'colored-element'){|args| args.last == 'background-red' }
  end

  it 'make red to me' do
    u = FactoryBot.create(:user, group: 'my-group')
    allow_any_instance_of(ApplicationHelper).to receive(:current_user).and_return(u)
    post channel_make_red_to_authenticated_group_path
    assert_action_to_authenticated_group('my-group', 'add_css_class'){|args| args == ["#colored-element", "background-red"] }
  end

  context 'class methods' do

    it 'class_method' do
      post channel_class_method_path
      assert_channel_to_all('class-method-box'){|ht| ht.inner_html.to_s.include?('Rendered from Class Method by Channel') }
    end

  end

end
