require 'rails_helper'

RSpec.describe "Articles", type: :request do

  let(:valid_params) { { book: { title: 'my-book' } } }
  let(:book) { FactoryBot.create :book }

  it 'expect updated' do
    u = FactoryBot.create :user
    patch book_path(book, params: valid_params)
    expect(turbo_targets.length).to eq(2)
    expect(all_turbo_responses.length).to eq(3)
    assert_channel_to_me(u, 'flash-box', count: 1) {|r|r.css('.callout.success').inner_html.include?('Book successfully updated')}
    assert_channel_to_me(u, 'flash-box', count: 1){|r|r.css('.callout.success').inner_html.include?('ADDITIONAL MESSAGE')}
    assert_action_to_me(u, 'add_css_class', count: 1){ |args|  args == ["#colored-element", "background-green"] }
    expect(session['last_saved_object']).to eq(book.to_global_id)
  end

  it 'test nil object should raise error' do
    expect{
      get test_nil_books_path
    }.to raise_exception(RuntimeError, "Could not fetch a model name by «eval(\"\@\#{controller_name.classify.underscore}\")». You must provide the argument :object.")
  end


end
