FactoryBot.define do
  factory :article do
    title { "MyText" }
    description { "MyText" }
  end
end
