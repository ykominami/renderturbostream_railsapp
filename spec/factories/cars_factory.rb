FactoryBot.define do
  factory :car do
    title { "MyText" }
    description { "MyText" }
  end
end
