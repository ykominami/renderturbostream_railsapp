require 'rails_helper'

RSpec.describe "target_id", type: :system do

  it 'by model' do
    art = FactoryBot.build(:article)
    expect(target_id('articles/_form', art)).to eq('new-article-form')
  end

  it 'by model' do
    art = FactoryBot.build(:article)
    expect(target_id_css('articles/_form', art)).to eq('#new-article-form')
  end

  it 'by model 2' do
    art = FactoryBot.build(:article)
    expect(target_id_css('_form', art)).to eq('#new-article-form')
  end

  it 'by model 2' do
    art = FactoryBot.build(:article)
    expect(target_id_css('form', art)).to eq('#new-article-form')
  end

end