require 'rails_helper'

RSpec.describe "svelte", type: :system do

  it 'check if svelte is running' do
    visit '/svelte'
    expect(page).not_to have_content('Hello Svelte-Tag')

    click_button 'push svelte-tag by turbo-stream'
    expect(page).to have_content('Hello Svelte-Tag by turbo-stream')

    click_button 'post with CSRF Token by axios'
    expect(page).not_to have_content('Hello Svelte-Tag by turbo-stream')
    expect(page).to have_content('Hello Svelte-Tag by channel')

  end
end