require 'rails_helper'

RSpec.describe "books", type: :system do

  it "create expect flash after creation" do
    FactoryBot.create(:user)
    visit '/cars/page'
    click_link 'New Book'

    title = SecureRandom.hex(8)
    within("#books-box") do
      fill_in 'Title', with: title
      click_button 'Create Book'
    end
    expect(page).to have_content title
    within(".callout.success") do
      expect(page).to have_content 'Book successfully created'
    end
    expect(page).to have_content 'Listing books'
  end

  it "expect additional flash and change class after update" do
    FactoryBot.create(:user)
    book = FactoryBot.create(:book, title: 'my-book')

    visit '/cars/page'

    expect(page).to have_css("#colored-element.background-green", count: 0)

    within("#books-box") do
      # sleep(0.6)
      click_link "Edit#{book.id}"
      click_button 'Update Book'
    end

    expect(page).to have_content 'Book successfully updated'
    expect(page).to have_content 'ADDITIONAL MESSAGE'
    expect(page).to have_content 'Rendered update template!'
    expect(page).to have_css("#colored-element.background-green", count: 1)
  end

end