require 'rails_helper'

RSpec.describe "form actions", type: :system do

  it "add css class" do
    visit '/turbo_power'
    expect(page).to have_css("#colored-element", count: 1)
    expect(page).to have_css("#colored-element.background-red", count: 0)
    click_button 'make red'
    expect(page).to have_css("#colored-element.background-red", count: 1)
  end

  it 'update history' do
    visit '/turbo_power'
    expect(page).to have_current_path('/turbo_power')
    click_button 'push_state'
    expect(page).to have_current_path('/my-custom-history')
  end




end