require 'rails_helper'

RSpec.describe "form actions", type: :system do

  it 'partial to all' do
    expect(page).not_to have_content /simple partial to all users rendered/
    visit '/channel/redirect'
    click_button 'partial to all'
    sleep(0.1)
    expect(page).to have_content /simple partial to all users rendered/
  end

  it 'template to all' do
    visit '/channel/redirect'
    expect(page).not_to have_content /rendered default template to all visitors/
    click_button 'template to all'
    sleep(0.1)
    expect(page).to have_content /rendered default template to all visitors/
  end

  it 'multiple to all' do
    visit '/channel/redirect'
    expect(page).not_to have_content /rendered one from more to all!/
    expect(page).not_to have_content /flash one from multiple to all/
    click_button 'multiple to all'
    sleep(0.1)
    expect(page).to have_content /rendered one from more to all!/
    expect(page).to have_content /flash one from multiple to all/
  end

  it 'red to all' do
    visit '/channel/redirect'
    expect(page).to have_css("#colored-element.background-red", count: 0)
    click_button 'background red to all'
    sleep(0.1)
    expect(page).to have_css("#colored-element.background-red", count: 1)
  end

  it 'green to all' do
    visit '/channel/redirect'
    expect(page).to have_css("#colored-element.background-green", count: 0)
    click_button 'background green to all'
    sleep(0.1)
    expect(page).to have_css("#colored-element.background-green", count: 1)
  end

  context 'authenticated' do

    # Devise here is not installed!
    # i was not able to mock the helper current_user
    # Reason is that the session variable is not working properly on capybara
    # this surely can be solved but i spent not more time there
    # methods like `render_to_me`, `action_to_authenticated_group` are now tested by the request helpers

    before(:each) do
      FactoryBot.create(:user, name: 'adrian', group: 'developer')
      visit '/users'
      expect(page).to have_content /login adrian/
      refresh
      visit current_path
      # click_link('login adrian')
      # expect(page).to have_content /You are logged in as adrian/
    end

    it 'partial to me' do
      # expect(page).not_to have_content /rendered default template to me/
      # visit '/channel/redirect'
      # click_button 'render to me'
      # sleep(0.1)
      # expect(page).to have_content /rendered default template to me/
    end
  end

end