require 'rails_helper'

RSpec.describe "form actions", type: :system do

  it 'turbo stream in haml' do
    visit root_path
    expect(page).to have_content("Listing articles")
    click_button 'check turbo-stream haml'
    expect(page).to have_content 'turbo-stream.haml is working!'
  end

  it 'turbo stream in erb' do
    visit root_path
    expect(page).to have_content("Listing articles")
    click_button 'check turbo-stream ERB'
    expect(page).to have_content 'turbo-stream.erb is working!'
  end

  it 'stream partials' do
    visit root_path
    click_button 'stream partials'
    expect(page).to have_content("FIRST streamed partial")
    expect(page).to have_content("SECOND streamed partial")
  end
end