require 'rails_helper'

RSpec.describe "Partials", type: :system do

  it 'channel locals test' do
    visit '/partials/index'
    expect(page).not_to have_content /Partial by text/
    click_button 'channel locals test'
    within '#render-to-box' do
      expect(page).to have_content /Partial by text\/html/
      expect(page).to have_content /locals\[:title\] => locals not working/
      expect(page).to have_content /local_assigns\[:title\] => «working»/
      expect(page).to have_content /title => «working»/
      expect(page).to have_content /@article.title => «Article.title is working»/
      expect(page).to have_content /@var => «hello»/
    end
  end

  it 'partial by stream' do
    visit '/partials/index'
    expect(page).not_to have_content /Partial by text/

    click_button 'partial by stream'

    within '#render-to-box' do
      expect(page).to have_content /local_assigns\[:title\] => «working»/
      expect(page).to have_content /title => «working»/
      expect(page).to have_content /@article.title => «Article.title is working»/
      expect(page).to have_content /@var => «working»/
    end
  end

end