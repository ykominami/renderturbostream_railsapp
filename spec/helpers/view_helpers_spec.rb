require 'rails_helper'

RSpec.describe ActionView, type: :helper do

  it 'empty' do
    expect(helper.last_saved_object).to eq(nil)
  end

  it 'senseless input' do
    session['last_saved_object'] = 'abc'
    expect(helper.last_saved_object).to eq(nil)
  end

  it 'success' do
    a = FactoryBot.create(:article)
    session['last_saved_object'] = a.to_global_id
    expect(helper.last_saved_object).to eq(a)
  end

end
