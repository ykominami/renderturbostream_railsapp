Rails.application.routes.draw do

  resources :books do
    collection do
      get :test_nil
    end
  end
  resources :users do
    member do
      post :login
    end
    collection do
      get :logout
    end
  end
  resources :articles do
    collection do
      post :check
      post :check_erb
      post :check_file
      post :test_stream_partial
      post :test_stream_partials
      post :test_no_stream
    end
  end

  get 'channel/redirect'
  get 'channel/redirect1'
  post 'channel/redirect1'
  get 'channel/redirect2'
  get 'channel/redirect3'
  post 'channel/redirect2'
  post 'channel/flash_test'
  post 'channel/make_red'
  post 'channel/make_red_to_me'
  post 'channel/make_red_to_authenticated_group'
  post 'channel/make_green'
  post 'channel/partial_locals_test'
  post 'channel/partial_to_all'
  post 'channel/template_to_all'
  post 'channel/template_to_me'
  post 'channel/template_to_authenticated_group'
  post 'channel/cable_template'
  post 'channel/multiple_to_all'
  post 'channel/class_method'

  post 'partials/test_stream_partial'
  post 'partials/test_channel'
  post 'partials/stream_by_params'
  post 'partials/channel_by_params'
  get 'partials/index'
  get 'partials/traditional'
  get 'partials/traditional2'

  resources :turbo_power do
    collection do
      post :action
    end
  end
  resources :cars do
    collection do
      get :page
    end
  end

  post 'svelte/push'
  get 'svelte/push'
  post 'svelte/push2'
  get 'svelte', to: 'svelte#index'

  root "articles#index"
end
