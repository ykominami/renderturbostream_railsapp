require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RenderTurboStream
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    config.x.render_turbo_stream.flash_partial = 'layouts/flash'
    config.x.render_turbo_stream.flash_target_id = 'flash-box'
    config.x.render_turbo_stream.flash_turbo_action = 'prepend'
    config.x.render_turbo_stream.allow_channel_to_me_for_turbo_stream_save = true

    config.x.render_turbo_stream.store_last_saved_object = true
  end
end
